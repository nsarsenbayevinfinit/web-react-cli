export { %component%State } from './state';
export { %component%Props } from './props';
export { %component% } from './%component%';
export { %component%View } from './%component%View';

export class %component%State {
    
    constructor(...inits: Partial<%component%State>[]) {
        Object.assign(this, ...inits);
    }
}
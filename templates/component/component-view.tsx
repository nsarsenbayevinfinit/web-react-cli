import React from 'react';
import './%component%.scss';

interface %component%ViewProps {

}

export const %component%View: React.SFC<%component%ViewProps> = (props) => {
    const { } = props;

    return (
        <div className="%className% alt_%className%">

        </div>
    );
};